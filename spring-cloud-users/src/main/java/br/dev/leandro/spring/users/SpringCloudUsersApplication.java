package br.dev.leandro.spring.users;

import lombok.extern.log4j.Log4j2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;

@Log4j2
@RefreshScope
@SpringBootApplication
public class SpringCloudUsersApplication {

    public static void main(String[] args) {
        log.info(":: Iniciando Spring-Cloud-Users ::");
        SpringApplication.run(SpringCloudUsersApplication.class, args);
        log.info(":: Spring-Cloud-Users iniciado com sucesso ::");
    }
}

